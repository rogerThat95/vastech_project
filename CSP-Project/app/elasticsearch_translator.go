package main

import (
	"net/url"
	"fmt"
	"net/http"
	"bytes"
	"sync"
	"strconv"
	"io"
	"time"
)



// basic template option which is not capable of performing aggregations on any fields except value and timestamp
var simpleTemplate = []byte(`
{
  "template": "metrics-*",
  "settings": {
    "index": {
      "refresh_interval": "5s"
    }
  },
  "mappings": {
    "point": {
      "_all":            { "enabled": false },
      "_source":         { "enabled": true },
      "properties": {
        "metric":	{"type": "text"},
        "value"	:	{"type": "double"},
        "host"  :       {"type": "text"},
         "os"   :	{"type": "text"},
        "timestamp":    { "type": "date", "doc_values": true }
      }
    }
  },
  "aliases" :{
   "es_stats" :{}
 }
}
`)


//optimized for aggregations using doc_values
var aggregationTemplate= []byte(`
{
  "template": "stats_*",
  "settings": {
    "index": {
      "refresh_interval": "5s",
      "number_of_replicas": "0",
      "number_of_shards": "1"
    }
  },
  "mappings": {
    "_default_": {
      "dynamic_templates": [
        {
          "all_string_fields_can_be_used_for_filtering": {
            "match": "*",
            "match_mapping_type": "string",
            "mapping": {
              "type": "string",
              "doc_values": true,
              "index": "not_analyzed"
            }
          }
        },
        {
          "all_nonstring_fields_are_just_stored_in_column_index": {
            "match": "*",
            "match_mapping_type": "*",
            "mapping": {
              "doc_values": true,
              "index": "not_analyzed"
            }
          }
        }
      ],
      "_all": { "enabled": false },
      "_source": { "enabled": false },
      "properties": {
        "timestamp": {
          "type": "date",
          "doc_values": true,
          "index": "not_analyzed"
        }

      }
    }
  },
   "aliases" :{
   "stats" :{}
 }
}
`)







// scratchBufPool helps reuse serialization scratch buffers. *coz we frequently allocate many objects of the same type and want to save some allocation and garbage collection overhead
var scratchBufPool = &sync.Pool{
	New: func() interface{} {
		return make([]byte, 0, 1024)
	},
}

func createESTemplate(daemonURL, templateName string, templateBody []byte) error {
	u, err := url.Parse( daemonURL)
	if err != nil {
		return err
	}

	u.Path = fmt.Sprintf("_template/%s", templateName)

	req, err := http.NewRequest("PUT", u.String(), bytes.NewReader(templateBody))
	if err != nil {
		return err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	// does the body need to be read into the void?

	if resp.StatusCode != 200 {
		return fmt.Errorf("bad mapping create")
	}
	return nil
}



func (p *Point)TranslatorESBulk(w io.Writer) error{ // this translator is set to make every field seperate i.e no taglist
	current_date := time.Now().Local()// get the current date for daily index creation
	index_name := "stats_"+current_date.Format("2006-01-02")

	buf := scratchBufPool.Get().([]byte)
	buf = append(buf, "{ \"index\" : { \"_index\" : \""...)
	buf = append(buf, index_name ...)
	buf = append(buf, "\", \"_type\" : \"point\" } }\n"...)

	buf = append(buf, '{')

	for i := 0; i < len(p.TagKeys); i++ {
		if i > 0 {
			buf = append(buf, ", "...)
		}
		buf = append(buf, "\""...)
		buf = append(buf, p.TagKeys[i]...)
		buf = append(buf, "\": \""...)
		buf = append(buf, p.TagValues[i]...)
		buf = append(buf, "\""...)
	}

	if len(p.TagKeys) > 0 && len(p.FieldKeys) > 0 {
		buf = append(buf, ',')
		buf = append(buf, ' ')
	}

	for i := 0; i < len(p.FieldKeys); i++ {
		if i > 0 {
			buf = append(buf, ", "...)
		}
		buf = append(buf, '"')
		buf = append(buf, p.FieldKeys[i]...)
		buf = append(buf, "\": "...)

		v := p.FieldValues[i]
		buf = fastFormatAppend(v, buf)
	}

	if len(p.TagKeys) > 0 || len(p.FieldKeys) > 0 {
		buf = append(buf, ", "...)
	}
	// Timestamps in ES must be millisecond precision:
	buf = append(buf, "\"timestamp\": "...)
	buf = fastFormatAppend(p.Timestamp.Local().UnixNano()/1e6, buf)
	buf = append(buf, " }\n"...)

	_, err := w.Write(buf)

	buf = buf[:0]
	scratchBufPool.Put(buf)

	return err
}



func (p *Point)TranslatorESBulkV2(w io.Writer) error{ // this tranlator is set to create a tag list of all the different tags in a metric
	current_date := time.Now().Local()// get the current date for daily index creation
	index_name := "stats_"+current_date.Format("2006-01-02")

	buf := scratchBufPool.Get().([]byte)
	buf = append(buf, "{ \"index\" : { \"_index\" : \""...)
	buf = append(buf, index_name ...)
	buf = append(buf, "\", \"_type\" : \"point\" } }\n"...)

	buf = append(buf, "{ \"taglist\":["...)

	for i := 0; i < len(p.TagKeys); i++ {
		if i > 0 {
			buf = append(buf, ", "...)
		}
		buf = append(buf, "\""...)
		buf = append(buf, p.TagKeys[i]...)
		buf = append(buf, "\": \""...)
		buf = append(buf, p.TagValues[i]...)
		buf = append(buf, "\""...)

	}
	buf = append(buf, "]"...)

	if len(p.TagKeys) > 0 && len(p.FieldKeys) > 0 {
		buf = append(buf, ',')
		buf = append(buf, ' ')
	}

	for i := 0; i < len(p.FieldKeys); i++ {
		if i > 0 {
			buf = append(buf, ", "...)
		}
		buf = append(buf, '"')
		buf = append(buf, p.FieldKeys[i]...)
		buf = append(buf, "\": "...)

		v := p.FieldValues[i]
		buf = fastFormatAppend(v, buf)
	}

	if len(p.TagKeys) > 0 || len(p.FieldKeys) > 0 {
		buf = append(buf, ", "...)
	}
	// Timestamps in ES must be millisecond precision:
	buf = append(buf, "\"timestamp\": "...)
	buf = fastFormatAppend(p.Timestamp.Local().UnixNano()/1e6, buf)
	buf = append(buf, " }\n"...)

	_, err := w.Write(buf)

	buf = buf[:0]
	scratchBufPool.Put(buf)

	return err
}


// make faster
func fastFormatAppend(v interface{}, buf []byte) []byte {
	switch v.(type) {
	case int:
		return strconv.AppendInt(buf, int64(v.(int)), 10)
	case int64:
		return strconv.AppendInt(buf, v.(int64), 10)
	case uint64:
		return strconv.AppendUint(buf, v.(uint64), 10)
	case float64:
		return strconv.AppendFloat(buf, v.(float64), 'f', 16, 64)
	case float32:
		return strconv.AppendFloat(buf, float64(v.(float32)), 'f', 16, 32)
	case bool:
		return strconv.AppendBool(buf, v.(bool))
	case []byte:
		buf = append(buf, v.([]byte)...)
		return buf
	case string:
		buf = append(buf, v.(string)...)
		return buf
	default:
		panic(fmt.Sprintf("unknown field type for %#v", v))
	}
}











