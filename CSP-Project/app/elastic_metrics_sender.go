package main


import (
	"bytes"
	"github.com/valyala/fasthttp"
	"sync"
	"time"

	"io"
	"fmt"
)




// Global vars
var (

	useGzip = true
	refreshEachBatch = true
	workersGroup sync.WaitGroup
	mutex = &sync.Mutex{}
	backingOffChan chan bool
	backingOffDone chan struct{}

)
type Report struct {
	numberOfMetrics int
	ingestion	float64
	latency		float64
}



func startESWorkers(totalESWorkers int){
 //starts worker that will be sending data to elastic
	for i := 0; i < totalESWorkers; i++ {
		daemonUrl := "http://localhost:9200"
		workersGroup.Add(1)
		cfg := HTTPWriterConfig{
			Host: daemonUrl,
		}

		go processBatches(NewHTTPWriter(cfg, refreshEachBatch),i) // all workers are independent of each other
	}

	//go processBackoffMessages()


}

// processBatches reads byte buffers from batchChan and writes them to the target server, while tracking stats on the write.
func processBatches(w *HTTPWriter,workerId int) {
	Ticker := time.NewTicker(time.Second*60)
	var totalMetrics int
	var totalLatency float64
	var totalIngestion float64
	var totalTimeTaken2 float64
	var totalSerialization float64
	//var workID int

	go func(){
	for t := range Ticker.C {
		compressedBatch := bufPoolES.Get().(*bytes.Buffer)
		reportBatch := bufPoolES .Get().(*bytes.Buffer) //buffer to write report metrics for each transaction performed
		fmt.Println("Metrics Report Sent", t)
		//fmt.Printf("Metrics per minute %dmetrics\n", totalMetrics)
		//fmt.Printf("Translation time per minute %fsec\n", totalSerialization/float64(totalMetrics))
		//fmt.Printf("Total Process time per minute %fsec\n", totalTimeTaken2/float64(totalMetrics))
		//fmt.Printf("Latency per minute %fsec\n", totalLatency/float64(totalMetrics))
		//fmt.Printf("Ingestion rate per minute %fmetrics/sec\n", float64(totalMetrics)/totalLatency)


                mutex.Lock()
		MetricReport(reportBatch,"ess.total.process.time.per.minute",totalMetrics,workerId,totalTimeTaken2/float64(totalMetrics))
		MetricReport(reportBatch,"ess.latency.per.minute",totalMetrics,workerId,totalLatency/float64(totalMetrics))
		MetricReport(reportBatch,"ess.total.latency.per.minute",totalMetrics,workerId,totalLatency)
		MetricReport(reportBatch,"ess.translation.time.per.minute",totalMetrics,workerId,totalSerialization/float64(totalMetrics))
		MetricReport(reportBatch,"ess.metrics.per.minute",totalMetrics,workerId,totalMetrics)
		MetricReport(reportBatch,"ess.ingestion.rate.per.minute",totalMetrics,workerId,float64(totalMetrics)/totalLatency)
		mutex.Unlock()
		//time.Sleep(time.Mil)
		fasthttp.WriteGzip(compressedBatch, reportBatch.Bytes())
		_, _ = w.WriteLineProtocol(compressedBatch.Bytes(), true)

		mutex.Lock()
		totalMetrics	= 0
		totalLatency    =0
		totalIngestion  = 0
		totalTimeTaken2 =0
		totalSerialization =0
		compressedBatch.Reset()
		bufPoolES.Put(compressedBatch)
		mutex.Unlock()
		//time.Sleep(time.Millisecond)

	}
}()


	for batch := range BatchESQueue {

		var err error
		var latency float64
		var ingestion float64

		// Write the batch.
		if useGzip {
			compressedBatch := bufPoolES.Get().(*bytes.Buffer)
			fasthttp.WriteGzip(compressedBatch, batch.data.Bytes())
			latency, err = w.WriteLineProtocol(compressedBatch.Bytes(), true)
			/*
			if err !=nil {
				backingOffChan <- true
				time.Sleep(backoff)
			} else {
				backingOffChan <- false
				break
			}
			*/
                        if err==nil{


				ingestion = float64(batch.numOfMetrics) / float64(latency)
				totalTimeTaken := float64(time.Since(batch.timeStart).Seconds())
				//fmt.Printf("The worker loaded %d metrics to ElasticSearch in %fsec at a mean rate of %f metrics/sec total time taken %fsec\n",batch.numOfMetrics,latency,ingestion,totalTimeTaken)
				mutex.Lock()
				totalMetrics += batch.numOfMetrics
				totalLatency += latency
				totalIngestion += ingestion
				totalTimeTaken2 +=totalTimeTaken
				totalSerialization +=batch.translationTime
				mutex.Unlock()
				time.Sleep(time.Millisecond)
				// Return the compressed batch buffer to the pool.
				compressedBatch.Reset()
				bufPoolES.Put(compressedBatch)
			}




		} else {
			latency, err = w.WriteLineProtocol(batch.data.Bytes(), false)
			ingestion = float64(batch.numOfMetrics) / float64(latency)
			totalTimeTaken := float64(time.Since(batch.timeStart).Seconds())

			totalMetrics += batch.numOfMetrics
			totalLatency += latency
			totalIngestion += ingestion
			totalTimeTaken2 +=totalTimeTaken
			totalSerialization +=batch.translationTime


		}

		if err != nil {
			fmt.Errorf("Error writing: %s\n", err.Error(),"App will now back off for 30 seconds")
			time.Sleep(time.Second*30)
			fmt.Println("Back off complete")
			//log.Fatalf()
		}


		// Return the batch buffer to the pool.
		batch.data.Reset()
		bufPoolES.Put(batch.data)
	}

	workersGroup.Done()
}

func processBackoffMessages() {
	var totalBackoffSecs float64
	var start time.Time
	last := false
	for this := range backingOffChan {
		if this && !last {
			start = time.Now()
			last = true
		} else if !this && last {
			took := time.Now().Sub(start)
			fmt.Printf("backoff took %.02fsec\n", took.Seconds())
			totalBackoffSecs += took.Seconds()
			last = false
			start = time.Now()
		}
	}
	fmt.Printf("backoffs took a total of %fsec of runtime\n", totalBackoffSecs)
	backingOffDone <- struct{}{}
}

func MetricReport(w io.Writer,reportMetric string,numOfMetrics int,workerID int,value interface{}) error{


	current_date := time.Now().Local()// get the current date for daily index creation
	index_name := "stats_"+current_date.Format("2006-01-02")


	buf := scratchBufPool.Get().([]byte)
	buf = append(buf, "{ \"index\" : { \"_index\" : \""...)
	buf = append(buf, index_name ...)
	buf = append(buf, "\", \"_type\" : \"point\" } }\n"...)

	buf = append(buf, '{')
	buf = append(buf, "\""...)
	buf = append(buf, "metric_type"...)
	buf = append(buf, "\": \""...)
	buf = append(buf, reportMetric ...)
	buf = append(buf, "\""...)
	buf = append(buf, ", "...)

	buf = append(buf, "\""...)
	buf = append(buf, "numberofMetrics"...)
	buf = append(buf, "\":"...)
	buf = fastFormatAppend(numOfMetrics, buf)
	buf = append(buf, ", "...)

	buf = append(buf, "\""...)
	buf = append(buf, "workerID"...)
	buf = append(buf, "\":"...)
	buf = fastFormatAppend(workerID, buf)
	buf = append(buf, ", "...)

	buf = append(buf, "\""...)
	buf = append(buf, "value"...)
	buf = append(buf, "\":"...)
	buf = fastFormatAppend(value, buf)
	buf = append(buf, ", "...)

	timestamp :=time.Now()
	buf = append(buf, "\"timestamp\": "...)
	buf = fastFormatAppend(timestamp.Local().UnixNano()/1e6, buf)
	buf = append(buf, " }\n"...)

	_, err := w.Write(buf)

	buf = buf[:0]
	scratchBufPool.Put(buf)

	return err




}









