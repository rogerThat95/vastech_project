package main


import (
	_"net/http"
	_"fmt"
	"io"
	"time"
	"bytes"
	"sync"
	"os"
	"log"
	_"github.com/buaazp/fasthttprouter"
	_"github.com/valyala/fasthttp"
	"net/http"
	"fmt"
)
// Default app variables

const  (
	// LocalDefaultURL is the default endpoint of Elasticsearch when running the application from local machine
	LocalDefaultURL = "http://localhost:9200"

	// ContainerDefaultURL is the default endpoint of Elasticsearch when running the application from a Go docker container
	ContainerDefaultURL = "http://elasticsearch:9200"
)

var (


	totalWorkers   		= 100 // total amount of workers in workpool receiving work requests
	totalESWorkers 		= 8//?
	totalOpenTSDBWorkers	=11
	maxQueueSize       		= 5000 // size of work que and batch que
	bufPoolES       	sync.Pool
	bufPoolOpenTSDB 	sync.Pool
	testingPool     	sync.Pool
	WorkerPool 		chan chan WorkRequest // workPool which will contain the workers with a channel to receive work fro,
	WorkQueue 		= make(chan WorkRequest,maxQueueSize)  // channel to send work requests through(buffered
	BatchESQueue 		= make(chan Batch,maxQueueSize )  //channel to send the batches serialized to ES format
	//BatchOpenTSDBQueue 	= make(chan Batch,maxQueue ) //channel to send the batches serialized to OpenTSDB Format

	//Delete
	testing io.Writer


)


type WorkRequest struct {//represents the propeties of the job request
    metricPointCreator  func([]Metric, int, time.Time) // the job to be run by each worker
    timestart	        time.Time // the time the metrics was received
    numberofMetrics	int  // total number of metrics received
    metrics 		[]Metric // metrics that will be processed
}

type Batch struct { // describes the properties of a typical batch

 data   *bytes.Buffer // this is the metrics translated/converted serialized in the respective format
 numOfMetrics int // total number of metrics translated/converted
 workerID     int // worker you processed the job
 timeStart time.Time // time the batch was received
 translationTime float64 // total time it took to translate the batch

}

type Worker struct{// describes the properties of a worker
        ID          int
	Work        chan WorkRequest // channel to receive work request
	WorkerPool  chan chan WorkRequest // channel that contains the workers and has another channel to receive work requests
	QuitChan    chan bool
}



//NewWorker creates, and returns a new Worker Object. Its only argument is
// a channel that the worker can add itself to whenever it is done its work
func NewWorker(id int, workerPool chan chan WorkRequest) Worker{
	// Create, and return the worker.
	worker := Worker{
		ID:          id,
		Work:        make(chan WorkRequest), //chanel for worker to receive the work requests on, worker handle one task at a time
		WorkerPool : workerPool,
		QuitChan:    make(chan bool)}

	return worker

}

// This function "starts" the worker by starting a goroutine, that is
// an infinite "for-select" loop.
func (w *Worker) Start(){
	go func (){
		for {
			//Add ourselves into the worker pool
			w.WorkerPool <- w.Work

			select {
			case work := <-w.Work:
			//fmt.Println("Worker",w.ID, "started processing work request")

			    metrics := work.metrics // take the slice of metrics that i received
			    work.metricPointCreator(metrics,w.ID,work.timestart) // process those metrics accordingly
			case <-w.QuitChan:
				// we have been asked to stop listen for workRequest
				//fmt.Printf("worker%d stopping\n", w.ID)
				return

			}

		}




	}()

}

// Stop tells the worker to stop listening for work requests.
// Note that the worker will only stop *after* it has finished its work.
func (w *Worker) Stop() {
	go func() { //wrapping in goroutine avoids the blocking nature of unbuffered channels, not block
		w.QuitChan <- true
	}()
}


func Dispatcher(totalWorkers int,totalESWorkers int){
	// start the workers who will be sending batches to elastic search
	startESWorkers(totalESWorkers)


	// start the workers who will be sending batches to opentsdb
	//startOpenTSDBWorkers(totalOpenTSDBWorkers)

	// First, initialize the channel we are going to but the workers' work channels into.
	WorkerPool = make(chan chan WorkRequest, totalWorkers)

	// Now, create all of our workers.
	for i := 0; i<totalWorkers; i++ {
		//fmt.Println("Starting worker", i+1)
		worker := NewWorker(i+1, WorkerPool)
		worker.Start()
	}
	go func() {
		for {
			select {
			case work := <-WorkQueue:
				//fmt.Println("Received work requeust")
				go func() {//The reason we send the work request to the worker in another goroutine, is so that we make sure the work queue never fills up
					worker := <-WorkerPool

					//fmt.Println("Dispatching work request")
					worker <- work
				}()
			}
		}
	}()


}


func main() { //main goroutine, creates and sets the ElasticTemplate and listens/receives (for) requests on specified port

	// create the index template:
	err := createESTemplate(LocalDefaultURL, "stats_template",  aggregationTemplate)
	if err != nil {
		log.Fatal(err)
	}

        // Pool that contains buffers that workers can take from to write the translated batches (this is done to reduce heap size)
	bufPoolES = sync.Pool{
		New: func() interface{} {
			return bytes.NewBuffer(make([]byte, 0, 4*1024*1024))
		},
	}

	bufPoolOpenTSDB= sync.Pool{
		New: func() interface{} {
			return bytes.NewBuffer(make([]byte, 0, 4*1024*1024))
		},
	}

	testingPool = sync.Pool{
		New: func() interface{} {
			file,_  := os.Create("esSData")
			//return make([]byte, 0, 1024)
			return file
		},
	}


	Dispatcher(totalWorkers,totalESWorkers)






		http.HandleFunc("/api/put/stat/metadata",metadata) // handler for loadgen metdata
		http.HandleFunc("/api/put/stat",loadGenMetricsCollector) // handler for loadgen metrics
		http.HandleFunc("/api/put",metricsSCollector) // handler for scollector metrics


		if err := http.ListenAndServe(":7000", nil); err != nil {//port to listen for requests
			fmt.Println(err.Error())
		}











}

