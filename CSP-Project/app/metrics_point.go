package main

import (
	"time"
	_"io"
	_"bytes"
	"bytes"

	"fmt"
)

type Point struct { // describes how a metric is broken down for simplicity of translating and putting into a bulk format
	MetricName      []byte
	TagKeys         [][]byte
	TagValues       [][]byte
	FieldKeys       [][]byte
	FieldValues     []interface{}
	Timestamp       *time.Time
}


func (p *Point) Reset() {
	p.MetricName  = nil
	p.TagKeys = p.TagKeys[:0]
	p.TagValues = p.TagValues[:0]
	p.FieldKeys = p.FieldKeys[:0]
	p.FieldValues = p.FieldValues[:0]
	p.Timestamp = nil
}

func (p *Point) SetTimestamp(t *time.Time) {
	p.Timestamp = t
}

func (p *Point) SetMetricName (s []byte) {
	p.MetricName  = s
}

func (p *Point) AppendTag(key, value []byte) {
	p.TagKeys = append(p.TagKeys, key)
	p.TagValues = append(p.TagValues, value)
}

func (p *Point) AppendField(key []byte, value interface{}) {
	p.FieldKeys = append(p.FieldKeys, key)
	p.FieldValues = append(p.FieldValues, value)
}







func metricPointCreator(metrics []Metric, workerID int, timeStart time.Time) {
	start := time.Now()
	batchES := bufPoolES .Get().(*bytes.Buffer)
	//testing := testingPool.Get().(io.Writer)
	//batchOpenTSDB := bufPoolOpenTSDB.Get().(*bytes.Buffer)
	counter :=0

	for key, _ := range metrics {

		p := Point{}
		p.Reset()// reset before processing metric(point)


		metric_type := metrics[key].Metric

			timestamp := metrics[key].Timestamp
			timestamp2 := time.Unix(timestamp, 0)

			value := metrics[key].Value
			tags := metrics[key].Tags
			p.SetMetricName([]byte(metric_type))
			p.SetTimestamp(&timestamp2)
			p.AppendTag([]byte("metric"),[]byte(metric_type))
			for k, v := range tags {
				p.AppendTag([]byte(k), []byte(v))
			}

			//p.AppendField([]byte("metric"), metric_type)
			p.AppendField([]byte("value"), value)

			// Call the respective translation methods
			p.TranslatorESBulk(batchES)




		       //  p.TranslatorESBulkV2(batchES)


			//p.TagKeys = append(p.TagKeys[:0],p.TagKeys[1:]...)
			//p.TagValues = append(p.TagValues[:0],p.TagValues[1:]...)
			//p.SerializeOpenTSDBBulk(batchOpenTSDB)//check performance
			counter++


	}
	translationTime := float64(time.Since(start).Seconds())

	esBatch := Batch{
		data: batchES,
		numOfMetrics:counter,
		workerID: workerID,
		timeStart:timeStart,
		translationTime:translationTime}
	/*
	opentsdbBatch := Batch{ //
		data: batchOpenTSDB,
		numOfMetrics:counter,
		workerID: workerID,
		timeStart:timeStart,
		translationTime:translationTime}
		*/

	if len(BatchESQueue) == maxQueueSize{
		fmt.Errorf("App Error:","ElasticSearch Que Full, Can't Receive more batches to send,failed to receive batches")

	}else{
		BatchESQueue <- esBatch
	}

           // send the batch to the ES batch queue
	//BatchOpenTSDBQueue <- opentsdbBatch //send the batch to the OpenTSDb batch queue


}


