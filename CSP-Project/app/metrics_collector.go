package main

import (
	"log"
	"encoding/json"
	"net/http"
	"compress/gzip"
	"fmt"
	"time"
)
type Metric struct { //metric json format
	Metric    string            `json.metric`
	Timestamp int64           `json.timestamp`
	Value     float64           `json.value`
	Tags      map[string]string `json.tags`

}

func loadGenMetricsCollector(w http.ResponseWriter, r *http.Request){
	receivedTime := time.Now()

	metrics := make([]Metric, 0)
	reader, err := gzip.NewReader(r.Body)

	if err != nil {

		log.Fatal(err)

	}

	defer reader.Close()

	decoder := json.NewDecoder(reader)

	err = decoder.Decode(&metrics)



	if err != nil {

		log.Fatal("Error decoding metrics in HTTP request body: ", err)

	}
	//sanity check
	if len(metrics) == 0{
		panic("No metrics were recieved")
	}
	//now, we take the desired slice of metrics, and the method/action to run them through and make a WorkRequest out of them
	work := WorkRequest{
		metricPointCreator: metricPointCreator,
		metrics:metrics,
		timestart:receivedTime ,
		numberofMetrics:len(metrics)}
	//Push the work onto the queue
	if len(WorkQueue) == maxQueueSize{
		fmt.Errorf("App Error:","Worke Que Full, Can't Receive more work,failed to receive work")

	}else{
		WorkQueue <- work
	}





}


func metricsSCollector(w http.ResponseWriter, r *http.Request){
	receivedTime := time.Now()
	metrics := make([]Metric, 0)
	reader, err := gzip.NewReader(r.Body)

	if err != nil {

		log.Fatal(err)

	}

	defer reader.Close()

	decoder := json.NewDecoder(reader)

	err = decoder.Decode(&metrics)


	if err != nil {

		log.Fatal("Error decoding metrics in HTTP request body: ", err)

	}
	//sanity check
	if len(metrics) == 0{
		panic("No metrics were recieved")
	}
	//now, we take the desired slice of metrics, the time the metrics were received and total metrics as well as
	// the method/action to run them through and make a WorkRequest out of them
	work := WorkRequest{
		metricPointCreator: metricPointCreator,
		metrics:metrics,
		timestart:receivedTime ,
		numberofMetrics:len(metrics)}
	//Push the work onto the queue

	if len(WorkQueue) == maxQueueSize{
		fmt.Errorf("App Error:","Worke Que Full, Can't Receive more work,failed to receive work")

	}else{
		WorkQueue <- work
	}




}

func metadata(w http.ResponseWriter, r *http.Request){
	 fmt.Fprint(w,r.Body)
	return
}
/*
func metadata(ctx *fasthttp.RequestCtx){
	fmt.Fprint(ctx,ctx.Response..Body)
	return
}
*/